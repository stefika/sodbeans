/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.quorum.projects;

/**
 *
 * @author stefika
 */
public enum QuorumProjectType {
    STANDARD,
    LEGO,
    WEB,
    WEB_BROWSER;
}
