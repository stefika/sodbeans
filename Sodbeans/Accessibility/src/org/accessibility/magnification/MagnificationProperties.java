/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.accessibility.magnification;

/**
 * A basic property file for handling magnification details in NetBeans.
 * 
 * @author Andreas Stefik
 */
public class MagnificationProperties {
    public static final float ZOOM_INCREASE = 1.25f;
}
