/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
@OptionsPanelController.ContainerRegistration(id = "Accessibility", categoryName = "#OptionsCategory_Name_Accessibility", iconBase = "org/accessibility/resources/Accessibility.png", keywords = "#OptionsCategory_Keywords_Accessibility", keywordsCategory = "Accessibility")
@NbBundle.Messages(value = {"OptionsCategory_Name_Accessibility=Accessibility", "OptionsCategory_Keywords_Accessibility=Accessibility, Speech"})
package org.accessibility.windows;

import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.NbBundle;
